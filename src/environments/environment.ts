// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBbH7yXomiQK0aQISBp5wj_14hdZ_maDxc',
    authDomain: 'controle-if97.firebaseapp.com',
    projectId: 'controle-if97',
    storageBucket: 'controle-if97.appspot.com',
    messagingSenderId: '1044343271911',
    appId: '1:1044343271911:web:1e391db4642166195ecd42',
    measurementId: 'G-P1K4GFXQ6J'
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
